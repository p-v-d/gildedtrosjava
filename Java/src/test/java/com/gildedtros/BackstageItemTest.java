package com.gildedtros;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BackstageItemTest {
	
	@Test
	void testVariableIncreasingQuality() {
        Item[] items = new Item[] { 
        		new Item("Backstage passes for Re:Factor", 11, 10),
        		new Item("Backstage passes for HAXX", 6, 17),
        		new Item("Backstage passes for Re:Factor", 15, 10),
        };
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Backstage passes for Re:Factor", app.items[0].name);
        assertEquals(10, app.items[0].sellIn);		
        assertEquals(11, app.items[0].quality);
        
        assertEquals("Backstage passes for HAXX", app.items[1].name);
        assertEquals(5, app.items[1].sellIn);		
        assertEquals(19, app.items[1].quality);
        
        assertEquals(14, app.items[2].sellIn);		
        assertEquals(11, app.items[2].quality);
        
        
        app.updateQuality();
        assertEquals(9, app.items[0].sellIn);		
        assertEquals(13, app.items[0].quality);
        
        assertEquals(4, app.items[1].sellIn);		
        assertEquals(22, app.items[1].quality);
        
        assertEquals(13, app.items[2].sellIn);		
        assertEquals(12, app.items[2].quality);
	}

	@Test
	void testQualityLimit() {
        Item[] items = new Item[] { 
        		new Item("Backstage passes for Re:Factor", 11, 48),
        		new Item("Backstage passes for HAXX", 6, 47),
        		new Item("Backstage passes for Re:Factor", 15, 49),
        };
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Backstage passes for Re:Factor", app.items[0].name);
        assertEquals(10, app.items[0].sellIn);		
        assertEquals(49, app.items[0].quality);
        
        assertEquals("Backstage passes for HAXX", app.items[1].name);
        assertEquals(5, app.items[1].sellIn);		
        assertEquals(49, app.items[1].quality);
        
        assertEquals(14, app.items[2].sellIn);		
        assertEquals(50, app.items[2].quality);
        
        
        app.updateQuality();
        assertEquals(9, app.items[0].sellIn);		
        assertEquals(50, app.items[0].quality);

        assertEquals(4, app.items[1].sellIn);		
        assertEquals(50, app.items[1].quality);
        
        assertEquals(13, app.items[2].sellIn);		
        assertEquals(50, app.items[2].quality);
	}
	
	@Test
	void testQualityReset() {
        Item[] items = new Item[] { 
        		new Item("Backstage passes for Re:Factor", 1, 47)
        };
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Backstage passes for Re:Factor", app.items[0].name);
        assertEquals(0, app.items[0].sellIn);		
        assertEquals(50, app.items[0].quality);
        
        app.updateQuality();
        assertEquals(-1, app.items[0].sellIn);		
        assertEquals(0, app.items[0].quality);
	}
}