package com.gildedtros;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DefaultItemTest {

	@Test
	void testQualityDecrease() {
        Item[] items = new Item[] { 
        		new Item("Normal", 5, 15),
        		new Item("The One Ring", 1, 45)
        		};
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Normal", app.items[0].name);
        assertEquals(4, app.items[0].sellIn);		
        assertEquals(14, app.items[0].quality);
        
        assertEquals(0, app.items[1].sellIn);		
        assertEquals(44, app.items[1].quality);
        
        
        app.updateQuality();
        assertEquals("Normal", app.items[0].name);
        assertEquals(3, app.items[0].sellIn);		
        assertEquals(13, app.items[0].quality);
        
        assertEquals(-1, app.items[1].sellIn);		
        assertEquals(42, app.items[1].quality);
	}
	
	@Test
	void testQualityLimit() {
        Item[] items = new Item[] { 
        		new Item("Normal", 1, 3),
        		new Item("The One Ring", 1, 2)
        		};
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Normal", app.items[0].name);
        assertEquals(0, app.items[0].sellIn);		
        assertEquals(2, app.items[0].quality);
        
        assertEquals(0, app.items[1].sellIn);		
        assertEquals(1, app.items[1].quality);
        
        
        app.updateQuality();
        assertEquals("Normal", app.items[0].name);
        assertEquals(-1, app.items[0].sellIn);		
        assertEquals(0, app.items[0].quality);
        
        assertEquals(-1, app.items[1].sellIn);		
        assertEquals(0, app.items[1].quality);
	}
}
