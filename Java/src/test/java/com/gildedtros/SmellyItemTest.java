package com.gildedtros;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SmellyItemTest {
	
	@Test
	void testQualityDecrease() {
        Item[] items = new Item[] { 
        		new Item("Duplicate Code", 5, 15),
        		new Item("Long Methods", 1, 45)
        		};
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Duplicate Code", app.items[0].name);
        assertEquals(4, app.items[0].sellIn);		
        assertEquals(13, app.items[0].quality);
        
        assertEquals("Long Methods", app.items[1].name);
        assertEquals(0, app.items[1].sellIn);		
        assertEquals(43, app.items[1].quality);
        
        
        app.updateQuality();
        assertEquals(3, app.items[0].sellIn);		
        assertEquals(11, app.items[0].quality);
        
        assertEquals(-1, app.items[1].sellIn);		
        assertEquals(39, app.items[1].quality);
	}
	
	@Test
	void testQualityLimit() {
        Item[] items = new Item[] { 
        		new Item("Ugly Variable Names", 1, 3),
        		new Item("Duplicate Code", 1, 2),
        		new Item("Long Methods", 1, 4),
        		new Item("Long Methods", 1, 1)
        		};
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Ugly Variable Names", app.items[0].name);
        assertEquals(0, app.items[0].sellIn);		
        assertEquals(1, app.items[0].quality);
        
        assertEquals("Duplicate Code", app.items[1].name);
        assertEquals(0, app.items[1].sellIn);		
        assertEquals(0, app.items[1].quality);
        
        assertEquals("Long Methods", app.items[2].name);
        assertEquals(0, app.items[2].sellIn);		
        assertEquals(2, app.items[2].quality);
        
        assertEquals("Long Methods", app.items[3].name);
        assertEquals(0, app.items[3].sellIn);		
        assertEquals(0, app.items[3].quality);
        
        app.updateQuality();
        assertEquals(-1, app.items[0].sellIn);		
        assertEquals(0, app.items[0].quality);
        
        assertEquals(-1, app.items[1].sellIn);		
        assertEquals(0, app.items[1].quality);
        
        assertEquals(-1, app.items[2].sellIn);		
        assertEquals(0, app.items[2].quality);
        
        assertEquals(-1, app.items[3].sellIn);		
        assertEquals(0, app.items[3].quality);
	}
}