package com.gildedtros;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LegendaryItemTest {
	
	@Test
	void testLegendaryItem() {
        Item[] items = new Item[] { new Item("B-DAWG Keychain", 35, 80) };
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals("B-DAWG Keychain", app.items[0].name);
        assertEquals(35, app.items[0].sellIn);		
        assertEquals(80, app.items[0].quality);
	}
}