package com.gildedtros;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class AgingItemTest {

	@Test
	void testIncreasingQuality() {
        Item[] items = new Item[] { new Item("Good Wine", 7, 45) };
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals("Good Wine", app.items[0].name);
        assertEquals(6, app.items[0].sellIn);		
        assertEquals(46, app.items[0].quality);		
	}
	
	@Test
	void testQualityLimit() {
        Item[] items = new Item[] { 
        		new Item("Good Wine", 7, 49),
        		new Item("Good Wine", 0, 48)
        		};
        GildedTros app = new GildedTros(items);
        
        app.updateQuality();
        assertEquals("Good Wine", app.items[0].name);
        assertEquals(6, app.items[0].sellIn);		
        assertEquals(50, app.items[0].quality);

        assertEquals(-1, app.items[1].sellIn);		
        assertEquals(50, app.items[1].quality);
        
        
        app.updateQuality();
        assertEquals("Good Wine", app.items[0].name);
        assertEquals(5, app.items[0].sellIn);		
        assertEquals(50, app.items[0].quality);
                
        assertEquals(-2, app.items[1].sellIn);		
        assertEquals(50, app.items[1].quality);	
	}
}