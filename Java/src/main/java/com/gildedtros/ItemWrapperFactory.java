package com.gildedtros;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.gildedtros.itemswrappers.AItemWrapper;
import com.gildedtros.itemswrappers.AgingItem;
import com.gildedtros.itemswrappers.BackstageItem;
import com.gildedtros.itemswrappers.DefaultItem;
import com.gildedtros.itemswrappers.LegendaryItem;
import com.gildedtros.itemswrappers.SmellyItem;

public class ItemWrapperFactory {
	
	private static final Map<String, String> itemTypes = Map.of(
			"Good Wine", AgingItem.class.getName(),
			"B-DAWG Keychain", LegendaryItem.class.getName(),
			"Backstage passes for Re:Factor", BackstageItem.class.getName(),
			"Backstage passes for HAXX", BackstageItem.class.getName(),
			"Duplicate Code", SmellyItem.class.getName(),
			"Long Methods", SmellyItem.class.getName(),
			"Ugly Variable Names", SmellyItem.class.getName()
	);
	
	public AItemWrapper getItemWrapper(Item legacyItem) {
		Class<?> type;
		
		try {
			type = Class.forName(itemTypes.get(legacyItem.name));
		} catch(NullPointerException | ClassNotFoundException e) {
			type = DefaultItem.class;
		}
		
		try {
			return (AItemWrapper) type.getConstructor(String.class, int.class, int.class).newInstance(legacyItem.name, legacyItem.sellIn, legacyItem.quality);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			//TODO: handle exception
			e.printStackTrace();
		}
		
		return null; //TODO: When exceptions are handled, don't throw a null
	}
}