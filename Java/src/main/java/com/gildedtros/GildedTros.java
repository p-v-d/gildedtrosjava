package com.gildedtros;

import com.gildedtros.itemswrappers.AItemWrapper;

class GildedTros {
    Item[] items;
	static ItemWrapperFactory itemFactory = new ItemWrapperFactory();
    
    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
    	for(Item legacyItem : items) {
    		AItemWrapper wrappedItem = itemFactory.getItemWrapper(legacyItem);
    		wrappedItem.updateItem();    		
    		legacyItem.sellIn = wrappedItem.getSellIn();
    		legacyItem.quality = wrappedItem.getQuality();
    	}
    }    
}