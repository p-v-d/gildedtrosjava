package com.gildedtros.itemswrappers;

public abstract class AItemWrapper {
	private final String name;
	protected int sellIn;
	protected int quality;
	
	public AItemWrapper(String name, int sellIn, int quality) {
		this.name = name;
		this.sellIn = sellIn;
		this.quality = quality;
	}
	
	public String getName() {
		return name;
	}
	
	public int getSellIn() {
		return sellIn;
	}
	
	public int getQuality() {
		return quality;
	}
	
	public abstract void updateItem();
}