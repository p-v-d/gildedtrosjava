package com.gildedtros.itemswrappers;

public class AgingItem extends AItemWrapper {

	public AgingItem(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}

	public void updateItem() {
		sellIn--;
		
		if (quality < 50) {
			quality++;
		}
		
		if (sellIn < 0 && quality < 50) {
			quality++;
		}
	}
}