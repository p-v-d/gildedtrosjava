package com.gildedtros.itemswrappers;

public class SmellyItem extends AItemWrapper {

	public SmellyItem(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}
	
	public void updateItem() {
		sellIn--;
		
		if (quality > 1) {
			quality -= 2;
		} else if (quality == 1) {
			quality--;
		}
		
		if (sellIn < 0 && quality > 1) {
			quality -= 2;
		} else if (sellIn < 0 && quality == 1) {
			quality--;
		}
	}
}