package com.gildedtros.itemswrappers;

public class LegendaryItem extends AItemWrapper {

	public LegendaryItem(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}
	
	public void updateItem() {}
}