package com.gildedtros.itemswrappers;

public class BackstageItem extends AItemWrapper {

	public BackstageItem(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}
	
	public void updateItem() {
		sellIn--;
		
		if (sellIn < 0) {
			quality = 0;
		} else {
			if (quality < 50) {
				quality++;	
			}
			
			if (sellIn < 10 && quality < 50) {
				quality++;
			}
			
			if (sellIn < 5 && quality < 50) {
				quality++;
			}
		}
	}
}
