package com.gildedtros.itemswrappers;

public class DefaultItem extends AItemWrapper {
	
	public DefaultItem(String name, int sellIn, int quality) {
		super(name, sellIn, quality);
	}
	
	public void updateItem() {
		sellIn--;
		
		if (quality > 0) {
			quality--;
		}
		
		if (sellIn < 0 && quality > 0) {
			quality--;
		}
	}
}
